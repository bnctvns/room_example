package com.bonioctavianus.roomexample

import com.google.gson.annotations.SerializedName

data class ApiResponse(@SerializedName("results") val results: List<People>)

data class People(@SerializedName("people_id") val id: String,
                  @SerializedName("name") val name: String,
                  @SerializedName("height") val height: String,
                  @SerializedName("hair_color") val hairColor: String,
                  @SerializedName("gender") val gender: String,
                  @SerializedName("films") val movies: List<Movie>)

data class Movie(@SerializedName("movie_id") val id: String,
                 @SerializedName("movie_url") val url: String)