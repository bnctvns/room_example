package com.bonioctavianus.roomexample

import android.arch.persistence.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class AppConverter {

    @TypeConverter
    fun moviesToString(movies: List<MovieEntity>): String = Gson().toJson(movies)

    @TypeConverter
    fun stringToMovies(string: String): List<MovieEntity> = Gson().fromJson(string, object : TypeToken<List<MovieEntity>>() {}.type);
}