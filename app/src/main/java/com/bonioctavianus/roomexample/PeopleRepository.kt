package com.bonioctavianus.roomexample

import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class PeopleRepository(val dao: PeopleDao) {

    fun insertPeoples(peoples: List<People>, callback: GenericCallback<List<Long>>): Disposable {
        return Single.fromCallable { dao.insertPeople(transformToPeopleEntity(peoples)) }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    callback.onSuccess(it)
                }, {
                    callback.onError(it)
                })
    }

    fun getPeoples(callback: GenericCallback<List<PeopleEntity>>): Disposable {
        return Single.fromCallable { dao.getPeoples() }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    callback.onSuccess(it)
                }, {
                    callback.onError(it)
                })
    }

    fun deletePeoples(callback: GenericCallback<Int>): Disposable {
        return Single.fromCallable { dao.deleteUsers() }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    callback.onSuccess(it)
                }, {
                    callback.onError(it)
                })
    }

    private fun transformToPeopleEntity(peoples: List<People>): List<PeopleEntity> {
        return peoples.flatMap {
            listOf(PeopleEntity(
                    id = it.id,
                    name = it.name,
                    height = it.height,
                    hairColor = it.hairColor,
                    gender = it.gender,
                    movies = transformToMovieEntity(it.movies)))
        }
    }

    private fun transformToMovieEntity(movies: List<Movie>): List<MovieEntity> = movies.flatMap { listOf(MovieEntity(id = it.id, url = it.url)) }
}
