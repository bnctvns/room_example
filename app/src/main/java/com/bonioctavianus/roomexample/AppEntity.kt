package com.bonioctavianus.roomexample

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.arch.persistence.room.TypeConverters

const val TABLE_PEOPLE_NAME = "peoples"
const val TABLE_MOVIE_NAME = "movies"

@Entity(tableName = TABLE_PEOPLE_NAME)
@TypeConverters(AppConverter::class)
data class PeopleEntity(@PrimaryKey
                        @ColumnInfo(name = "id") var id: String,
                        @ColumnInfo(name = "name") var name: String,
                        @ColumnInfo(name = "height") var height: String,
                        @ColumnInfo(name = "hair_color") var hairColor: String,
                        @ColumnInfo(name = "gender") var gender: String,
                        @ColumnInfo(name = "movies") var movies: List<MovieEntity> = emptyList<MovieEntity>()) {

    override fun toString(): String = name
}

data class MovieEntity(@ColumnInfo(name = "id") var id: String,
                       @ColumnInfo(name = "url") var url: String) {

    override fun toString(): String = url
}
