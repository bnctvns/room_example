package com.bonioctavianus.roomexample

import java.io.InputStream

class Helper {

    companion object {
        fun inputStreamToString(inputStream: InputStream): String? {
            try {
                val bytes = ByteArray(inputStream.available())
                inputStream.read(bytes, 0, bytes.size)
                val json = String(bytes)
                return json
            } catch (e: Throwable) {
                return null
            }
        }
    }
}
