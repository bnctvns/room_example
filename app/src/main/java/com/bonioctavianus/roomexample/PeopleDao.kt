package com.bonioctavianus.roomexample

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query

@Dao
interface PeopleDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertPeople(peoples: List<PeopleEntity>): List<Long>

    @Query("SELECT * FROM ".plus(TABLE_PEOPLE_NAME))
    fun getPeoples(): List<PeopleEntity>

    @Query("DELETE FROM ".plus(TABLE_PEOPLE_NAME))
    fun deleteUsers(): Int

}
