package com.bonioctavianus.roomexample

interface PeopleView {

    fun onInsertPeoplesSuccess(result: List<Long>)

    fun onInsertPeopleFailure(error: String)

    fun onGetPeoplesSuccess(peoples: List<PeopleEntity>)

    fun onGetPeoplesFailure(error: String)

    fun onDeletePeoplesSuccess(result: Int)

    fun onDeletePeoplesFailure(error: String)
}