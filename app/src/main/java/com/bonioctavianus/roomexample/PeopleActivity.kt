package com.bonioctavianus.roomexample

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class PeopleActivity : AppCompatActivity(), PeopleView {

    @Inject lateinit var repository: PeopleRepository
    private lateinit var presenter: PeoplePresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        (application as App).appComponent.inject(this)

        presenter = PeoplePresenter(this, repository)

        val stringResponse = Helper.inputStreamToString(resources.openRawResource(R.raw.response))
        val response = Gson().fromJson(stringResponse, ApiResponse::class.java)

        btn_insert_table.setOnClickListener { presenter.insertPeoples(response) }
        btn_query_table.setOnClickListener { presenter.getPeoples() }
        btn_drop_table.setOnClickListener { presenter.deletePeoples() }
    }

    override fun onInsertPeoplesSuccess(result: List<Long>) {
        presenter.getPeoples()
    }

    override fun onInsertPeopleFailure(error: String) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show()
    }

    override fun onGetPeoplesSuccess(peoples: List<PeopleEntity>) {
        var label = ""
        peoples.forEach { label += ("\n".plus(it.name).plus("\n").plus(it.movies.fold("") { sum, next -> sum.plus(next).plus("\n") })) }
        tv_text.text = if (!label.isNullOrBlank()) label else "No Data"
    }

    override fun onGetPeoplesFailure(error: String) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show()
    }

    override fun onDeletePeoplesSuccess(result: Int) {
        presenter.getPeoples()
    }

    override fun onDeletePeoplesFailure(error: String) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDestroy()
    }
}
