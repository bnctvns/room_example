package com.bonioctavianus.roomexample

import io.reactivex.disposables.CompositeDisposable

class PeoplePresenter(val view: PeopleView, val repository: PeopleRepository) {

    private val compositeDisposable = CompositeDisposable()

    fun insertPeoples(response: ApiResponse) {
        compositeDisposable.add(repository.insertPeoples(response.results, object : GenericCallback<List<Long>> {
            override fun onSuccess(result: List<Long>) {
                view.onInsertPeoplesSuccess(result)
            }

            override fun onError(error: Throwable) {
                view.onInsertPeopleFailure(error.message ?: "Insert Failure...")
            }
        }))
    }

    fun getPeoples() {
        compositeDisposable.add(repository.getPeoples(object : GenericCallback<List<PeopleEntity>> {
            override fun onSuccess(result: List<PeopleEntity>) {
                view.onGetPeoplesSuccess(result)
            }

            override fun onError(error: Throwable) {
                view.onGetPeoplesFailure(error.message ?: "Query Failure...")
            }
        }))
    }

    fun deletePeoples() {
        compositeDisposable.add(repository.deletePeoples(object : GenericCallback<Int> {
            override fun onSuccess(result: Int) {
                view.onDeletePeoplesSuccess(result)
            }

            override fun onError(error: Throwable) {
                view.onDeletePeoplesFailure(error.message ?: "Delete Failure...")
            }
        }))
    }

    fun onDestroy() {
        compositeDisposable.clear()
    }
}
