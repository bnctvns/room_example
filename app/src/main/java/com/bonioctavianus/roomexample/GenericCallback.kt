package com.bonioctavianus.roomexample

interface GenericCallback<T> {

    fun onSuccess(`result`: T)

    fun onError(error: Throwable)
}