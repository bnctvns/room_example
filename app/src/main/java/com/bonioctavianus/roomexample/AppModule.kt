package com.bonioctavianus.roomexample

import android.arch.persistence.room.Room
import android.content.Context
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(val context: Context) {

    @Provides
    @Singleton
    fun provideContext() = context

    @Provides
    @Singleton
    fun provideAppDatabase() = Room.databaseBuilder(context, AppDatabase::class.java, "database_name").build()

    @Provides
    @Singleton
    fun providePeopleDao(database: AppDatabase) = database.peopleDao()

    @Provides
    @Singleton
    fun providePeopleRepository(dao: PeopleDao) = PeopleRepository(dao)

}